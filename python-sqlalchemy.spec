%global __provides_exclude_from ^(%{python2_sitearch}|%{python3_sitearch})/.*\\.so$

Name:           python-sqlalchemy
Version:        1.2.11
Release:        2
Summary:        SQL toolkit and object relational mapper for Python
License:        MIT
URL:            http://www.sqlalchemy.org/
Source0:        https://files.pythonhosted.org/packages/source/S/SQLAlchemy/SQLAlchemy-%{version}.tar.gz

BuildRequires:  python2-devel >= 2.6 python2-setuptools python2-mock python2-pytest
BuildRequires:  python3-devel python3-setuptools python3-pytest

%description
SQLAlchemy is an Object Relational Mapper (ORM) that provides a flexible,
high-level interface to SQL databases. It contains a powerful mapping layer
that users can choose to work as automatically or as manually, determining
relationships based on foreign keys or to bridge the gap between database
and domain by letting you define the join conditions explicitly.

%package help
Summary:       Help documents for SQLAlchemy
BuildArch:     noarch
Provides:      %{name}-doc = %{version}-%{release}
Obsoletes:     %{name}-doc < %{version}-%{release}

%description help
Help documents for SQLAlchemy.

%package -n python2-sqlalchemy
Summary:       SQL toolkit and object relational mapper for Python
%{?python_provide:%python_provide python2-sqlalchemy}

%description -n python2-sqlalchemy
SQLAlchemy is an Object Relational Mapper (ORM) that provides a flexible,
high-level interface to SQL databases. It contains a powerful mapping layer
that users can choose to work as automatically or as manually, determining
relationships based on foreign keys or to bridge the gap between database
and domain by letting you define the join conditions explicitly.

The python2-sqlalchemy package contains the python 2 version of the module.

%package -n python3-sqlalchemy
Summary:        SQL toolkit and object relational mapper for Python
%{?python_provide:%python_provide python%{python3_pkgversion}-sqlalchemy}

%description -n python3-sqlalchemy
SQLAlchemy is an Object Relational Mapper (ORM) that provides a flexible,
high-level interface to SQL databases. It contains a powerful mapping layer
that users can choose to work as automatically or as manually, determining
relationships based on foreign keys or to bridge the gap between database
and domain by letting you define the join conditions explicitly.

The python3-sqlalchemy package contains the python 3 version of the module.

%prep
%autosetup -n SQLAlchemy-%{version} -p1

%build
%py2_build

%py3_build

%install
%py2_install

%py3_install

rm -rf doc/build

%check
PYTHONPATH=. %{__python2} -m pytest test

PYTHONPATH=. %{__python3} -m pytest test

%files -n python2-sqlalchemy
%license LICENSE
%doc README.rst
%{python2_sitearch}/*

%files -n python3-sqlalchemy
%license LICENSE
%doc README.rst
%{python3_sitearch}/*

%files help
%doc doc examples

%changelog
* Tue Nov 26 2019 yanzhihua <yanzhihua4@huawei.com> - 1.2.11-2
- Package init
